export default {
  define: {
    'process.env.ENV': 'test',
    'process.env.REDIRECT_URL':
      'https://siemensbudgetaryapi.azurewebsites.net/mobile/index.html',
    'process.env.Base': 'https://siemensbudgetaryapi.azurewebsites.net',
    'process.env.CLIENT_ID': '87987862-b622-4e1b-81eb-4f99c99916ff',
    'process.env.TENANTID': '44c24f42-d49b-4192-9336-5f2989b87356',
    'process.env.SCOPE': 'api://87987862-b622-4e1b-81eb-4f99c99916ff/default',
  },
};
