declare module "react-gantt-chart" {
  import * as React from "react";

  export interface GanttChartProps {
    data: Array<{
      id: string;
      name: string;
      start: string;
      end: string;
    }>;
    dateFormat?: string;
    cellWidth?: number;
    cellHeight?: number;
  }

  export default class GanttChart extends React.Component<GanttChartProps, any> {}
}
