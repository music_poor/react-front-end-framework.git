// @ts-nocheck
import {defineConfig} from "umi";

export default defineConfig({
  history: {type: "hash"},
  hash: true,
  npmClient: "yarn",
  plugins: ["@umijs/plugins/dist/dva"],
  dva: {},
  mfsu: {
    includes: ["antd"], // 添加这一行
  },
  //  proxy: {
  //   "/api": {
  //     target: "https://lorealworkcenter.azurewebsites.net",
  //     // pathRewrite: { '^/api': '' },
  //     changeOrigin: true,
  //     secure: false,
  //     pathRewrite: {
  //       "^/api": "/api",
  //     },
  //   },
  // },
  extraPostCSSPlugins: [
    require("postcss-preset-env")({
      stage: 3,
      features: {
        "nesting-rules": true,
      },
    }),
    require("postcss-px-to-viewport-next")({
      unitToConvert: "px", //需要转换的单位，默认为"px"；
      viewportWidth: 1440, // 根据设计搞来
      // viewportHeight: 1080,
      unitPrecision: 6,
      viewportUnit: "vw",
      propList: ["*"], //要进行转换的属性列表,*表示匹配所有,!表示不转换
      // selectorBlackList: ['.ignore', '.hairlines'],
      selectorBlackList: ["wrap"], //不进行转换的css选择器，继续使用原有单位
      minPixelValue: 1,
      mediaQuery: true, //设置媒体查询里的单位是否需要转换单位
      replace: true, //是否直接更换属性值，而不添加备用属性
      // exclude: /(\/|\\)(node_modules)(\/|\\)/, //忽略某些文件夹下的文件
    }),
  ],
  chainWebpack(config, {webpack}) {
    // 处理 JavaScript 文件
    config.optimization.splitChunks({
      cacheGroups: {
        async: {
          name: "chunk",
          chunks: "all",
          enforce: true,
          minChunks: 2,
          minSize: 0,
          // test: /\.(css|less)$/,
        },
      },
    });
    // 处理 CSS 文件
    config.plugin("extract-css").use(require("mini-css-extract-plugin"), [
      {
        filename: `css/[name].[contenthash:8].css`,
        chunkFilename: `css/[name].[contenthash:8].chunk.css`,
        ignoreOrder: true,
      },
    ]);
    config.output
      // JS 文件名添加 hash 值
      .chunkFilename("js/[name].[hash:8].chunk.js")
      .end();
  },
});
