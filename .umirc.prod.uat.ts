export default {
  define: {
    'process.env.ENV': 'uat',
    'process.env.REDIRECT_URL': 'https://diat-mobile-bot-test.siemens-healthineers.cn',
    'process.env.Base': 'https://diat-mobile-bot-test.siemens-healthineers.cn',
    'process.env.CLIENT_ID': '92d5cd70-e899-4f00-98f4-f0bf95dde929',
    'process.env.TENANTID': '5dbf1add-202a-4b8d-815b-bf0fb024e033',
    'process.env.SCOPE': 'api://4839a57f-9979-47a3-9c92-a372700137e6/user_impersonation',
  },
};
