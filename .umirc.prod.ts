export default {
  define: {
    'process.env.ENV': 'prod',
    'process.env.REDIRECT_URL': 'https://diat-mobile-bot.siemens-healthineers.cn',
    'process.env.Base': 'https://diat-mobile-bot.siemens-healthineers.cn',
    'process.env.CLIENT_ID': '7a5bf17f-94c9-4814-86b9-997b6061db56',
    'process.env.TENANTID': '5dbf1add-202a-4b8d-815b-bf0fb024e033',
    'process.env.SCOPE': 'api://2c4667f6-885f-47d2-8cbf-cc3eef989d63/user_impersonation',
  },
};
