# react前端框架

#### 介绍
react前端框架基于umi

#### 软件架构说明
- umi4+react18+less+ts+dva+axios
- 登录方式
  1. 自定义登录
  2. 集成msal azuse登录 //登录方式两种 loginPopup、loginRedirect
  3. loginPopup 配置文件-->utils/azureAuth.js
  4. loginRedirect 配置文件-->layouts/msalConfig.js
- 代码规则集成eslint+.prettier+stylelint+editorconfig
- 多环境管理
- 多语言
- 支持多布局控制 配置文件-->layouts/components/defaultSettings.ts
#### 项目初始化

1.  yarn || npm install

#### 启动

1.  yarn start

#### 打包

1.  yarn run build:test  //测试环境
1.  yarn run build:uat  //预发布环境
1.  yarn run build:prod  //生产环境

#### 特技

1. 实现甘特图 npm install react-gantt-chart  

2. UMI4 自定义多语言，3的插件@umijs/plugin-locale,4不支持需自定义 yarn add react-intl @formatjs/intl-locale

#### 技巧
1. // @ts-ignore --关闭ts 检查某个地方的代码
2. umi4中使用做vwvh适配postcss-px-to-viewport-next postcss-preset-env 
3. 多行文本 css 省略号   
    display: -webkit-box;
    height: 76px;
    overflow: hidden;
    line-height: 18px;
    text-overflow: ellipsis;
    -webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
4. react-flow-renderer 流程图
