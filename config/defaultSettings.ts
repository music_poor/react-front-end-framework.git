const proSettings = {
  // 改成白色
  navTheme: 'light',
  // 拂晓蓝
  primaryColor: '#1890ff',
  layout: 'side',
  contentWidth: 'Fluid',
  fixedHeader: false,
  fixSiderbar: true,
  colorWeak: false,
  internationalData: navigator.language,//'en-US',
  menu: {
    locale: true,
  },
  title: 'JIM',
  pwa: false,
  iconfontUrl: '',
};
export default proSettings;
