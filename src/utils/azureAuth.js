import {UserAgentApplication} from "msal";

const msalConfig = {
  auth: {
    clientId: process.env.CLIENT_ID,
    authority: "https://login.microsoftonline.com/44c24f42-d49b-4192-9336-5f2989b87356",
    redirectUri: process.env.REDIRECT_URL,
    scope: process.env.SCOPE,
    postLogoutRedirectUri: "http://localhost:8020/#/login", // 添加此行以配置注销后的重定向页面
  },
  cache: {
    cacheLocation: "sessionStorage",
    storeAuthStateInCookie: true,
  },
};

export const msalInstance = new UserAgentApplication(msalConfig);
// 获取token
export async function getAccessToken() {
  try {
     const account = msalInstance.getAccount();

    if (!account) {
      throw new Error('用户未登录');
    }
    const request = {
      // scopes: ['openid', 'profile', 'User.Read'], // 添加你需要的权限
      scopes: [process.env.SCOPE],
      account,
    };

    const response = await msalInstance.acquireTokenSilent(request);
    return response.accessToken;
  } catch (error) {
    console.error("获取访问令牌失败", error);
    throw error;
  }
}
