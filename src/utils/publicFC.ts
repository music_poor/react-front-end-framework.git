import { type MessageArgsProps, message } from 'antd';

import _ from 'lodash';

import * as FileSaver from 'file-saver';
import { type ReactElement, type JSXElementConstructor, type ReactFragment, type ReactPortal } from 'react';
// 过滤对象数据

export function removeProperty(obj: any) {

  Object.keys(obj)?.forEach((item: any) => {

    if (

      obj[item] === '' ||

      obj[item] === `${[]}` ||

      obj[item] === `${{}}` ||

      obj[item] === undefined ||

      obj[item] === null ||

      obj[item] === 'null'

    )

      delete obj[item];

  });

  return obj;

}

// 数组过滤假值 false, null,0, “”, undefined, 和 NaN

export function removeArray(arr: string[]) {

  const obj = _.compact(arr);

  return obj;

}

// null ,undefined,“”

export function onFilterArr(arr: any[]) {

  return arr.filter(function (s: string) {

    return s?.trim();

  });

}

export function onError(val: string | number | boolean | ReactElement<any, string | JSXElementConstructor<any>> | ReactFragment | ReactPortal | MessageArgsProps | null | undefined) {

  void message.error(val);

}

// 成功

export function onSuccess(val: string | number | boolean | ReactFragment | ReactPortal | MessageArgsProps | ReactElement<any, string | JSXElementConstructor<any>> | null | undefined) {

  void message.success(val);

}

// btoa()：字符串或二进制值转为Base64编码

// atob()：Base64编码转为原来的编码

// 字符串转base64

export function encode(str: string) {

  // 对字符串进行编码

  const encode = encodeURI(str);

  // 对编码的字符串转化base64

  const base64 = btoa(encode);

  return base64;

}

// base64转字符串

export function decode(base64: string) {

  // 对base64转编码

  const decode = atob(base64);

  // 编码转字符串

  const str = decodeURI(decode);

  return str;

}

export function intPartFormat(val: { toString: () => string; }) {

  if (!val) {

    return '0';

  }

  const intPartFormat = val.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,'); // 将整数部分逢三一断

  return intPartFormat;

}

// 设置奇偶行颜色

export function setRowClassName(index: number) {

  const rowColor = index % 2 === 0 ? '' : 'even'; // 判断单双行，赋予不同样式

  return rowColor;

}

// JavaScript 的字符串截取方法来截取字符串，匹配中文逗号和英文逗号

export function substringBeforeComma(str: string) {

  // 匹配中文逗号和英文逗号

  const regex = /,|，/;

  const arr = str?.split(regex);

  return removeArray(arr);

}

export function substringBefore(str: string) {

  // 匹配中文逗号和英文逗号

  const regex = /,|，/;

  const arr = str?.split(regex);

  return removeArray(arr);

}

// 数组对象进行去重

export function uniqueArr(arr: any[]) {

  const uniqueArr = arr?.filter((item, index) => {

    return (

      index ===

      arr?.findIndex((obj) => {

        return JSON.stringify(obj) === JSON.stringify(item);

      })

    );

  });

  return uniqueArr;

}

// 数组对象针对特定字段进行去重

export function uniqueByProperty(array: any[], property: string | number) {

  return array?.filter(

    (obj, index, self) =>

      index === self?.findIndex((t) => t[property] === obj[property]),

  );

}

// 提取数组对象到新数组并去重

export function newArray(array: any[], property: string | number) {

  return array?.map((obj) => obj[property]);

}

// 下载excel

export function onFileSaver(data: any, name: any) {

  const blob = b64toBlob(data, 'application/vnd.ms-excel');

  FileSaver.saveAs(blob, `${name}.xlsx`);

}

export function b64toBlob(b64Data: string, contentType: string) {

  contentType = contentType || '';

  const sliceSize = 512;

  const byteCharacters = atob(b64Data);

  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {

    const slice = byteCharacters?.slice(offset, offset + sliceSize);

    const byteNumbers = new Array(slice.length);

    for (let i = 0; i < slice.length; i++) {

      byteNumbers[i] = slice.charCodeAt(i);

    }

    const byteArray = new Uint8Array(byteNumbers);
    // @ts-ignore
    byteArrays.push(byteArray);

  }

  const blob = new Blob(byteArrays, { type: contentType });

  return blob;

}

// 对象去除假值

export const filterObject = (obj: Record<string, unknown> | ArrayLike<unknown>) => {

  return Object.entries(obj).reduce((acc, [key, value]) => {

    if (

      value !== false &&

      value !== null &&

      value !== 0 &&

      value !== '' &&

      value !== undefined &&

      !Number.isNaN(value)

    ) {

      acc[key] = value;

    }

    return acc;

  }, {});

};
