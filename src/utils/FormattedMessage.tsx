// 国际化公共方法
import { FormattedMessage, useIntl } from 'react-intl';


// 或使用 intl.formatMessage 方法
export function MenuItemWithIntlFormatMessage(val:any) {
  const intl = useIntl();
  const name = intl.formatMessage({ id: val });
  return name;
}


export function intlFormatMessage(val:any) {
  // 使用您的逻辑根据 id 格式化消息，而不使用钩子
    // 例如，您可以使用 react-intl 中的 `formatMessage` 函数
   return <FormattedMessage id={val }/>;
 }
