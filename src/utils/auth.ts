// import Cookies from 'js-cookie';
const TokenKey = 'token';
const UserKey = 'USER';
const MenusVal = 'menusVal';
const Internationalization = 'internationalization'; // 国际化
const RefershToken = 'refershToken'; // 刷新
export function getToken() {
  let token:any = '';
  if (token === '') {
    token = sessionStorage.getItem(TokenKey);
  }
  const Token = token;
  return Token;
}

export function getUser() {
  return sessionStorage.getItem(UserKey);
}

export function setToken(token: string) {
  sessionStorage.setItem(TokenKey, token);
  // Cookies.set(TokenKey, token);

}
export function getRefershToken() {
  return localStorage.getItem(RefershToken);
}

export function setRefershToken(token: string) {
  localStorage.setItem(RefershToken, token);

}
export function setMenus(menus: string) {
  sessionStorage.setItem(MenusVal, menus);

}
export function getMenus() {
  return sessionStorage.getItem(MenusVal);
}
export function setUser(user: string) {
  sessionStorage.setItem(UserKey, user);

}
// 国际化
export function getInternationalization() {
  return sessionStorage.getItem(Internationalization);
}
export function setInternationalization(val: string) {
  sessionStorage.setItem(Internationalization, val);
}

export function removeToken() {
  // Cookies.remove(TokenKey);
  // Cookies.remove(UserKey);
  // Cookies.remove(MenusVal);
  // Cookies.remove(Internationalization);
  localStorage.clear();
  sessionStorage.clear();
  location.reload();
  clearAllCookie();

}
export function ADremoveToken() {
  // Cookies.remove(TokenKey);
  // Cookies.remove(UserKey);
  // Cookies.remove(MenusVal);
  // Cookies.remove(Internationalization);
  localStorage.clear();
  localStorage.removeItem('pathname');
  localStorage.removeItem('search');
  localStorage.removeItem('refershToken');
  sessionStorage.clear();
  clearAllCookie();

}
// 清除所有cookie函数
function clearAllCookie() {
  const keys = document.cookie.match(/[^ =;]+(?==)/g);
  if (keys) {
    for (let i = keys.length; i--; )
      document.cookie = keys[i] + '=0;expires=' + new Date(0).toUTCString();
  }
}
