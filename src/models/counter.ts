import proSettings from "../../config/defaultSettings"
export default {
  namespace: "counter",

  state: {
    routeName:'',
    count: 0,
    token: "",
    installType: false,
    account: null,
    international:proSettings?.internationalData // 国际化
  },

  reducers: {

    routeName(state, { payload: routeName }) {
      return { ...state, routeName };
    },

    add(state, {payload: count}) {
      return {...state, count};
    },

    minus(state, {payload: count}) {
      return {...state, count};
    },
    setToken(state: any, {payload: token}: any) {
      return {...state, token};
    },
    installType(state, {payload: installType}: any) {
      return {...state, installType};
    },
    setInternational(state, {payload: international}) {
      return {...state, international};
    },
    // 退出初始化
    resetState(state) {
      return {
        ...state,
        count: 0,
        token: "",
        account:null,
        installType: false,
        international:proSettings?.internationalData
      };
    },
    // 刷新保存
    updateData(state, {payload}) {
      return {...state, ...payload};
    },
    // 用户信息
    saveAccount(state, { payload }) {
      return { ...state, account: payload };
    },
  },
  effects: {
    *saveData({payload}, {put, select}) {
      yield put({type: "updateData", payload});
      const data = yield select((state: any) => state.counter);
      sessionStorage.setItem("counterData", JSON.stringify(data));
    },
    *logout(_, {put}) {
      yield put({type: "resetState"});
    },
    *setAccount({ payload }, { put }) {
      yield put({ type: 'saveAccount', payload });
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      const data = sessionStorage.getItem("counterData");
      if (data) {
        dispatch({type: "saveData", payload: JSON.parse(data)});
      }
      // 监听 window.beforeunload 事件
      window.addEventListener("beforeunload", () => {
        dispatch({type: "saveData"});
      });
      if (history?.location?.pathname === '/login') {
        dispatch({type: "logout"});
        sessionStorage.clear()
      }
    },
  },
};
export interface RootState {
  counter: CounterState;
}

interface CounterState {
  count: number;
  token: string;
  installType: boolean;
  account: any; // 或用具体的类型替换 "any"
  international: string;
}
