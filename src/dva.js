import { create } from 'dva-core';
// import { createLogger } from 'redux-logger';
import createLoading from 'dva-loading';
import { message } from 'antd';

// 1. 创建 dva 实例
const app = create({
  onError(e) {
    message.error(e.message, 3);
  },
});

// 2. 使用插件
// app.use(createLogger());
app.use(createLoading());

// 3. 注册模型
app.model(require('./models/counter').default);

// 4. 导出 dva 实例
export default app;
