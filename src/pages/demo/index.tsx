import {useState} from "react";
import "./DragAndDrop.css";

const DragAndDrop = () => {
  const [leftItems, setLeftItems] = useState([
    // {id: 1, text: "Item 1"},
    // {id: 2, text: "Item 2"},
    // {id: 3, text: "Item 3"},
    {
      id: "materiaL_NO",
      text: "商品编号",
    },
    {
      id: "brand",
      text: "品牌代码",
    },
    {
      id: "brandname",
      text: "品牌名称",
    },
    {
      id: "barcode",
      text: "条形码",
    },
    {
      id: "batcH_NO",
      text: "批次号",
    },
    {
      id: "materiaL_DES",
      text: "产品名称",
    },
    {
      id: "delNo",
      text: "交货单号",
    },
    {
      id: "systemType",
      text: "是否进口",
    },
    {
      id: "soldtO_CODE",
      text: "售达方代码",
    },
    {
      id: "qty",
      text: "发货数量",
    },
    {
      id: "mfG_DATE",
      text: "生效时间",
    },
    {
      id: "expirY_DATE",
      text: "失效时间",
    },
    {
      id: "wH_NO",
      text: "发货工厂",
    },
    {
      id: "pushRuleStatus",
      text: "推送状态",
    },
    {
      id: "boM_MAT_NO",
      text: "套盒物料号",
    },
    {
      id: "boM_BATCH",
      text: "套盒批号",
    },
    {
      id: "boM_BARCODE",
      text: "套盒条形码",
    },
    {
      id: "bom_flag",
      text: "套盒标识",
    },
    {
      id: "boM_MFG_DATE",
      text: "套盒生产日期",
    },
    {
      id: "boM_EXPIRY_DATE",
      text: "套盒失效日期",
    },
    {
      id: "comP_MAT_NO",
      text: "组件物料号",
    },
    {
      id: "comP_BarCode",
      text: "组件条形码",
    },
    {
      id: "comP_BATCH",
      text: "组件批号",
    },
    {
      id: "comP_MFG_DATE",
      text: "组件生效日期",
    },
    {
      id: "comP_EXPIRY_DATE",
      text: "组件失效日期",
    },
    {
      id: "planneD_PGI_DATE",
      text: "预计发货日期",
    },
    {
      id: "actuaL_PGI_DATE",
      text: "实际发货日期",
    },
  ]);

  const [rightItems, setRightItems] = useState([]);

  const onDragStart = (event, item) => {
    event.dataTransfer.setData("text/plain", JSON.stringify(item));
  };

  const onDragOver = event => {
    event.preventDefault();
  };

  const onDrop = (event, targetItems, setTargetItems) => {
    event.preventDefault();
    const sourceItem = JSON.parse(event.dataTransfer.getData("text/plain"));

    if (!targetItems.find(item => item.id === sourceItem.id)) {
      setTargetItems([...targetItems, sourceItem]);
      setLeftItems(leftItems.filter(item => item.id !== sourceItem.id));
    } else {
      const targetElement = event.target.closest(".item");
      if (targetElement) {
        const targetId = targetElement.getAttribute("data-id");
        const sourceIndex = targetItems.findIndex(item => item.id === sourceItem.id);
        const targetIndex = targetItems.findIndex(item => item.id === targetId);
        const newItems = [...targetItems];
        newItems.splice(sourceIndex, 1);
        newItems.splice(targetIndex, 0, sourceItem);
        setTargetItems(newItems);
      }
    }
  };

  const onDelete = item => {
    setRightItems(rightItems.filter(rightItem => rightItem.id !== item.id));
    setLeftItems([...leftItems, item]);
  };

  return (
    <div className='container'>
      <div
        className='box'
        id='leftBox'
        onDragOver={onDragOver}
        onDrop={event => onDrop(event, leftItems, setLeftItems)}
      >
        {leftItems.map(item => (
          <div
            key={item.id}
            className='item'
            draggable='true'
            onDragStart={event => onDragStart(event, item)}
            data-id={item.id}
          >
            {item.text}
          </div>
        ))}
      </div>
      <div
        className='box'
        id='rightBox'
        onDragOver={onDragOver}
        onDrop={event => onDrop(event, rightItems, setRightItems)}
      >
        {rightItems.map(item => (
          <div
            key={item.id}
            className='item'
            draggable='true'
            onDragStart={event => onDragStart(event, item)}
            data-id={item.id}
          >
            {item.text}
            <button onClick={() => onDelete(item)} className='delete-btn'>
              Delete
            </button>
          </div>
        ))}
      </div>
    </div>
  );
};

export default DragAndDrop;
