import {useEffect} from "react";
import {history} from "umi";

export default function RedirectPage() {
  useEffect(() => {
    history.push("/login");
  }, []);

  return false;
}
