// AudioVisualizer.js
import styled from "@emotion/styled";
import {useEffect, useRef} from "react";

const Canvas = styled.canvas`
  width: 100%;
  height: 100%;
`;

const AudioVisualizer = ({audioData}) => {
  const canvasRef = useRef(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");
    // 在此处实现可视化逻辑
    const width = canvas.width;
    const height = canvas.height;
    ctx.clearRect(0, 0, width, height);

    ctx.lineWidth = 2;
    ctx.strokeStyle = "rgba(128, 128, 128, 0.5)";
    ctx.beginPath();

    const sliceWidth = width / audioData.length;
    let x = 0;

    for (const item of audioData) {
      const y = ((item / 255.0) * height) / 2;
      ctx.lineTo(x, y);
      x += sliceWidth;
    }

    ctx.lineTo(x, height / 2);
    ctx.stroke();
  }, [audioData]);

  return <Canvas ref={canvasRef} />;
};

export default AudioVisualizer;
