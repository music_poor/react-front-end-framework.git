import {Table} from "antd";
// import "antd/dist/antd.css";
import "./verticalTable.less";
const dataSource = [
  {
    key: "1",
    name: "John Doe",
    age: 32,
    address: "New York",
  },
  {
    key: "2",
    name: "Jane Smith",
    age: 28,
    address: "Los Angeles",
  },
];

const columns = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
    className: "vertical-header",
    render: (text, record) => <div data-title='Name'>{text}</div>,
  },
  {
    title: "Age",
    dataIndex: "age",
    key: "age",
    className: "vertical-header",
    render: (text, record) => <div data-title='Age'>{text}</div>,
  },
  {
    title: "Address",
    dataIndex: "address",
    key: "address",
    className: "vertical-header",
    render: (text, record) => <div data-title='Address'>{text}</div>,
  },
];

const App = () => {
  return <Table dataSource={dataSource} columns={columns} />;
};

export default App;
