import * as d3 from "d3";
import {useEffect, useRef} from "react";

// eslint-disable-next-line react/prop-types
const GanttChart = ({data, width = 1000, height = 500}) => {
  const svgRef = useRef();

  useEffect(() => {
    const svg = d3.select(svgRef.current);
    const margin = {top: 20, right: 20, bottom: 20, left: 100};
    const chartWidth = width - margin.left - margin.right;
    const chartHeight = height - margin.top - margin.bottom;

    const x = d3.scaleTime().range([0, chartWidth]);
    const y = d3.scaleBand().range([0, chartHeight]).padding(0.1);

    x.domain([d3.min(data, d => d.start), d3.max(data, d => d.end)]);

    y.domain(data.map(d => d.name));

    const xAxis = d3.axisTop(x).tickSize(-chartHeight);
    const yAxis = d3.axisLeft(y);

    svg.select(".x-axis").remove();
    svg.select(".y-axis").remove();

    svg.append("g").attr("class", "x-axis").attr("transform", `translate(${margin.left}, ${margin.top})`).call(xAxis);

    svg.append("g").attr("class", "y-axis").attr("transform", `translate(${margin.left}, ${margin.top})`).call(yAxis);

    const bars = svg.selectAll(".bar").data(data);

    bars
      .enter()
      .append("rect")
      .attr("class", "bar")
      .merge(bars)
      .attr("x", d => x(d.start) + margin.left)
      .attr("y", d => y(d.name) + margin.top)
      .attr("width", d => x(d.end) - x(d.start))
      .attr("height", y.bandwidth());

    bars.exit().remove();
  }, [data]);

  return <svg ref={svgRef} width={width} height={height} />;
};

export default GanttChart;
