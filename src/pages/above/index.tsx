// import {useEffect, useState} from "react";
// import {useLocation} from "umi";
import GanttChart from "./GanttChart.tsx";
const data = [
  {name: "Task A", start: new Date(2021, 0, 1), end: new Date(2021, 0, 5)},
  {name: "Task B", start: new Date(2021, 0, 3), end: new Date(2021, 0, 8)},
  {name: "Task C", start: new Date(2021, 0, 6), end: new Date(2021, 0, 12)},
];
const home = () => {
  // const {search} = useLocation();
  // const searchParams = new URLSearchParams(search);
  // const [visible, setVisible] = useState(false);
  // const [selectValue, setSelectValue] = useState<string>();
  // console.log(searchParams.get("type"));
  // useEffect(() => {}, []);

  return (
    <>
      <div>d3.JS 实现甘特图</div>
      <GanttChart data={data} />
    </>
  );
};
export default home;
