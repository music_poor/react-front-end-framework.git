import * as d3 from "d3";
import React, {useEffect, useRef} from "react";

interface GanttChartProps {
  data: Array<{
    name: string;
    start: Date;
    end: Date;
  }>;
}

const GanttChart: React.FC<GanttChartProps> = ({data}) => {
  const ganttChartRef = useRef(null);

  useEffect(() => {
    const svg = d3.select(ganttChartRef.current);

    const width = +svg.attr("width");
    const height = +svg.attr("height");

    const margin = {top: 20, right: 20, bottom: 20, left: 20};
    const innerWidth = width - margin.left - margin.right;
    const innerHeight = height - margin.top - margin.bottom;

    const xScale = d3
      .scaleTime()
      .domain([d3.min(data, d => d.start) as Date, d3.max(data, d => d.end) as Date])
      .range([0, innerWidth]);

    const yScale = d3
      .scaleBand()
      .domain(data.map(d => d.name))
      .range([0, innerHeight])
      .padding(0.1);

    const g = svg.append("g").attr("transform", `translate(${margin.left},${margin.top})`);

    g.append("g").attr("class", "x-axis").attr("transform", `translate(0,${innerHeight})`).call(d3.axisBottom(xScale));

    g.append("g").attr("class", "y-axis").call(d3.axisLeft(yScale));

    g.selectAll(".bar")
      .data(data)
      .enter()
      .append("rect")
      .attr("class", "bar")
      .attr("x", d => xScale(d.start))
      .attr("y", d => yScale(d.name) as number)
      .attr("width", d => xScale(d.end) - xScale(d.start))
      .attr("height", yScale.bandwidth());
  }, [data]);

  return <svg ref={ganttChartRef} width='1000' height='500' />;
};

export default GanttChart;
