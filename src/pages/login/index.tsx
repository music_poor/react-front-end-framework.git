import {onError} from "@/utils/publicFC";
// import {useMsal} from "@azure/msal-react";
import {handleLogin} from "@/layouts/msalConfig";
import {Button, Form, Input, Row} from "antd";
import React, {useEffect} from "react";
import {history} from "umi";
import styles from "./style.less";
const Login: React.FC = () => {
  useEffect(() => {
    form.setFieldsValue({
      username: "JIM",
      password: "Z.f@202208",
    });
  }, []);

  const [form] = Form.useForm();

  const onFinish = (values: any) => {
    if (values) {
      history.push("/home");
    } else {
      onError("请输入用户名或密码！");
    }
  };
  const onAdd = () => {
    handleLogin();
  };

  // 弹框登录
  // const handleLogin = () => {
  //   msalInstance
  //     .loginPopup()
  //     .then((response: any) => {
  //       void getAccessToken().then(res => {
  //         if (res) {
  //           // 使用 setAccessToken action 更新全局状态
  //           // dispatch({type: "global/setAccessToken", payload: {accessToken: res}});
  //           history.push("/home");
  //         }
  //       });
  //     })
  //     .catch((error: any) => {
  //       void message.error(error);
  //     });
  // };

  const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
  };

  const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
  };

  return (
    <div className={styles.box}>
      <Form
        form={form}
        {...layout}
        style={{minWidth: 600}}
        initialValues={{remember: true}}
        onFinish={onFinish}
        autoComplete='off'
        size={'large'}
      >
        <Form.Item label='用户名' name='username' rules={[{required: true, message: "请输入用户名!"}]}>
          <Input placeholder='请输入用户名' />
        </Form.Item>

        <Form.Item label='密码' name='password' rules={[{required: true, message: "请输入密码!"}]}>
          <Input.Password placeholder='请输入密码' />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Row>
            <Button size={'large'} block type='primary' htmlType='submit'>
              登录
            </Button>
          </Row>
          <Row>
            <Button size={'large'} block htmlType='button' onClick={onAdd} className={styles.rightButton}>
              AAD登录
            </Button>
          </Row>

        </Form.Item>
      </Form>
    </div>
  );
};

export default Login;
