import { MenuItemWithIntlFormatMessage } from "@/utils/FormattedMessage";
import {connect} from "dva";
interface CounterProps {
  count: number;
  dispatch: Function;
}
const Counter: React.FC<CounterProps> = ({count, dispatch}) => (
  <div>

    <h2>{MenuItemWithIntlFormatMessage('num')}{count}</h2>
    <button onClick={() => dispatch({type: "counter/add",payload:count+1})} style={{marginRight:'10px'}}>+</button>

    <button onClick={() => dispatch({type: "counter/minus",payload:count-1})}>-</button>
  </div>
);

export default connect(({counter}) => counter)(Counter);
