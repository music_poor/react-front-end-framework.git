import {MsalAuthenticationTemplate} from "@azure/msal-react";

const AuthCallback = () => {
  const authRequest = {
    scopes: [process.env.SCOPE], // 替换为你需要的权限
  };

  return (
    <MsalAuthenticationTemplate interactionType='redirect' authenticationRequest={authRequest}>
      <div>Loading...</div>
    </MsalAuthenticationTemplate>
  );
};

export default AuthCallback;
