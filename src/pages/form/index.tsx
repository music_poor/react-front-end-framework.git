import {connect} from "dva";
interface CounterProps {
  count: number;
  num: number;
  title: string;
  dispatch: Function;
}
const Form: React.FC<CounterProps> = ({num, title, count, dispatch}) => {
  return (
    <>
      <div>{title}</div>
      <div>{num}</div>
      <h2>公共{count}</h2>
    </>
  );
};
// export default Form;
export default connect(({form, counter}) => ({...form, ...counter}))(Form);
