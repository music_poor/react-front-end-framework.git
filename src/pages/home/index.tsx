import {useState} from "react";
import ReactFlow, {Controls, MarkerType, MiniMap, ReactFlowProvider} from "react-flow-renderer";
import "react-flow-renderer/dist/style.css";
const initialNodes = [
  {
    id: "1",
    type: "input",
    data: {label: "Input Node"},
    position: {x: 250, y: 25},

    style: {backgroundColor: "#6ede87", color: "white"},
  },

  {
    id: "2",
    // you can also pass a React component as a label
    data: {label: <div>Default Node</div>},
    position: {x: 100, y: 125},
  },
  {
    id: "3",
    type: "output",
    data: {label: "drag me around 😎"},
    position: {x: 400, y: 125},
  },
];

const initialEdges = [
  {
    id: "e1-2",
    source: "1",
    type: "step",
    target: "2",
    label: "This edge can only be updated from source",
    labelBgStyle: {fill: "#FFCC00", color: "#fff", fillOpacity: 0.7},
    animated: true,
    markerEnd: {
      type: MarkerType.Arrow,
    },
  },
  {
    id: "e2-3",
    type: "step",
    source: "1",
    target: "3",
    animated: true,
    markerEnd: {
      type: MarkerType.ArrowClosed,
    },
    style: {stroke: "red"},
  },
];
const nodeColor = node => {
  switch (node.type) {
    case "input":
      return "#6ede87";
    case "output":
      return "#6865A5";
    default:
      return "#ff0072";
  }
};
function Flow() {
  const [nodes, setNodes] = useState(initialNodes);
  const [edges, setEdges] = useState(initialEdges);

  return (
    <ReactFlowProvider>
      <ReactFlow nodesConnectable={false} defaultNodes={nodes} defaultEdges={edges} fitView>
        <Controls />
        <MiniMap nodeColor={nodeColor} nodeStrokeWidth={3} />
      </ReactFlow>
    </ReactFlowProvider>
  );
}

export default Flow;
