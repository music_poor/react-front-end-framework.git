import {msalInstance} from "@/layouts/msalConfig";
import {MsalProvider} from "@azure/msal-react";
import React from "react";
import {Provider} from "react-redux";
import {IntlProviderWrapper} from "./IntlProviderWrapper";
import app from "./dva";

app.start();

export function render(oldRender: () => void) {
  oldRender();
}
// 获取 international 数据
// const internationalData = app._store.getState().global.international;

export function rootContainer(
  container:
    | string
    | number
    | boolean
    | React.ReactElement<any, string | React.JSXElementConstructor<any>>
    | React.ReactFragment
    | React.ReactPortal
    | null
    | undefined
) {

  return React.createElement(

    IntlProviderWrapper,
    // @ts-ignore
    {},
    React.createElement(
      // 自定义登录删除第二层React.createElement，MsalProvider为aad的登录使用
      MsalProvider,
      {
        instance: msalInstance,
      },
      // 此处不可删除
      React.createElement(Provider, {store: app._store, children: container})
    )
  );

}
