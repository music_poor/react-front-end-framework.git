import {post,get} from './request';
import { Interface } from './Interface';
const baseURL = process.env.Base;

export const GetECMClientRelationByECMClientId = async (date:any) => {
  return await get(
    `${baseURL}${Interface.ReflushToken}?ECMClientId=${date}`,
  );
};

export const setSubmitQuotation = async (formData: object) => {
  return await post(
    `${baseURL}${Interface.ReflushToken}`,
    formData,
  );
};
