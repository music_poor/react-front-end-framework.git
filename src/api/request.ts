import axios from 'axios';

axios.defaults.headers.post['Content-Type'] = 'application/json';

const service = axios.create({
  baseURL: process.env.REACT_APP_BASE_API,
  timeout: 5000
});

// 请求拦截器
service.interceptors.request.use(
  config => {
    // 在请求头中添加token信息，可以根据具体业务场景进行修改
    // 如果已经存在了Authorization字段，则不需要再添加token信息
    if (!config.headers.Authorization) {
      const token = localStorage.getItem('token');
      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
    }

    return config;
  },
  error => {
    void Promise.reject(error);
  }
);

// 响应拦截器
service.interceptors.response.use(
  async response => {
    const { status, data } = response;
    if (status === 200) {
      // 根据后端返回数据格式进行具体的处理
      if (data.code === 0) {
        return await Promise.resolve(data.data);
      } else {
        const message = data.msg || '未知错误';
        return await Promise.reject(new Error(message));
      }
    } else {
      return await Promise.reject(response);
    }
  },
  async error => {
    let message = error.message;
    if (error.response?.data) {
      const errData = error.response.data;
      message = errData.msg || '未知错误';
    }
    return await Promise.reject(new Error(message));
  }
);

/**
 * get请求
 * @param {string} url 请求地址
 * @param {object} params 查询参数
 */
export async function get(url: string, params = {}) {
  return await service.get(url, {
    params
  });
}

/**
 * post请求
 * @param {string} url 请求地址
 * @param {object} data 提交的数据
 */
export async function post(url: string, data = {}) {
  return await service.post(url, data);
}

/**
 * put请求
 * @param {string} url 请求地址
 * @param {object} data 提交的数据
 */
export async function put(url: string, data = {}) {
  return await service.put(url, data);
}

/**
 * delete请求
 * @param {string} url 请求地址
 * @param {object} params 查询参数
 */
export async function del(url: string, params = {}) {
  return await service.delete(url, {
    params
  });
}
