import styled from "styled-components";
interface BackgroundDivProps {
  backgroundimg: string;
}
export const BackgroundDiv = styled.div<BackgroundDivProps>`
    height: 100%;
     background: url('${props => props.backgroundimg}') no-repeat;
    background-size:100% 100%;
    padding:20px
`;
