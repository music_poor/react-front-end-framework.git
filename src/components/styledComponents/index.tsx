import styled from "styled-components";
interface BackgroundDivProps {
  backgroundimg: string;
}
// 此处注意 vwvh 不能处理styled-components样式转换
export const BackgroundDiv = styled.div<BackgroundDivProps>`
    height: 100%;
     background: url('${props => props.backgroundimg}') no-repeat;
    background-size:100% 100%;
`;
