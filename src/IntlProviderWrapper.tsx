import React, { useState, useContext } from 'react';
import { IntlProvider } from 'react-intl';
import enUS from './locales/en-US';
import zhCN from './locales/zh-CN';
import { LanguageContext } from './LanguageContext';
import app from './dva';
const messages = {
  'en-US': enUS,
  'zh-CN': zhCN,
};

interface IntlProviderWrapperProps {
  children: React.ReactNode;
}

export const IntlProviderWrapper: React.FC<IntlProviderWrapperProps> = ({
  children,
}) => {
  const [language, setLanguage] = useState(app._store.getState().counter.international);
  console.log(language,'app')
  return (

    // 不刷新切换国际化 useContext传参
    <LanguageContext.Provider value={{ language, setLanguage }}>

      {/* 国际化 */}
      <IntlProvider locale={language} messages={messages[language]}>
        {children}
      </IntlProvider>

    </LanguageContext.Provider>

  );
};
