import { createContext } from 'react';
import proSettings from "../config/defaultSettings"
export const LanguageContext = createContext<{
  language: string;
  setLanguage: (language: string) => void;
}>({
  language: proSettings?.internationalData,
  setLanguage: () => {},
});
