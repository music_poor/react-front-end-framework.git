import styled from 'styled-components';

export const ContainerDiv = styled.div`

  background-size: 100%;
  background-color: #ececec;
  padding: 20px;
  height: 100%;
`;
export const WrapperDiv = styled.div`
  background-color: #fff;
  border-radius: 10px;
`;
export const SearchDiv = styled.div`
  padding: 30px 0px 0px;
  margin: 0 20px;
  border-bottom: 1px solid #eaf4f6;
`;
export const TableWrapDiv = styled.div`
  padding: 15px 20px;
  .stripe,
  .stripe .ant-table-cell-fix-right {
    background-color: #f9f9f9;
  }

  .ant-table {
    color: #666;
  }
  .ant-table-tbody > tr > td {
    border: none;
  }
`;
export const TableTopDiv = styled.div`
  overflow: hidden;
  margin-bottom: 10px;
`;
export const TableTitleDiv = styled.div`
  float: left;
  font-size: 16px;
  font-weight: bold;
`;
export const TableTitleSpanDiv = styled.div`
  height: 8px;
  background: #9ed0dd;
  margin-top: -9px;
`;
export const TaleTitleIconDiv = styled.div`
  margin-right: 10px;
  display: inline-block;
`;
export const TableBtnDiv = styled.div`
  float: right;
`;
export const BtnGreenWrap = styled.span`
  .ant-btn-link,
  .ant-btn-link:hover,
  .ant-btn-link:focus {
    color: #389e0d !important;
  }
`;
export const BtnOrgWrap = styled.span`
  .ant-btn-link,
  .ant-btn-link:hover,
  .ant-btn-link:focus {
    color: #ec6602 !important;
  }
`;
export const BtnBlaWrap = styled.span`
  .ant-btn-link,
  .ant-btn-link:hover,
  .ant-btn-link:focus {
    color: #333 !important;
  }
`;
export const BtnBlueWrap = styled.span`
  .ant-btn {
    background: rgb(64, 163, 189);
    color: #fff;
    border-color: rgb(64, 163, 189);
  }
  .ant-btn:hover,
  .ant-btn:focus {
    border-color: rgb(64, 163, 189);
  }
`;
export const OperDiv = styled.div`
  text-align: center;
  .ant-btn-link {
    color: rgb(64, 163, 189);
  }
  .ant-btn-link span {
    text-decoration: underline;
  }
  .ant-btn-dangerous.ant-btn-link {
    color: #ff4d4f;
  }
`;
