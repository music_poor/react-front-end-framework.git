import styled from 'styled-components';
export const WelComeDiv = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  padding: 80px 80px 150px;
  background-color: #ececec;
`;
export const ContentDiv = styled.div`
  height: 100%;
  background: url(${() => require(`@/assets/images/welcome_bg.png`)}) no-repeat
      bottom,
    linear-gradient(to bottom, #f3f3f3, #ececec);
  border-radius: 10px;
  min-height: 600px;
  position: relative;
`;
export const MessageDiv = styled.div`
  width: 380px;
  height: 195px;
  background: url(${() => require(`@/assets/images/message_bg.png`)}) no-repeat;
  position: absolute;
  top: 20%;
  left: 50%;
  padding: 30px;
  .welcomeHead {
    font-size: 26px;
    color: #333;
    margin: 5px 0 5px 12px;
    img {
      margin: -7px 0 0 8px;
    }
  }
  .welcomeTitle {
    font-size: 26px;
    font-weight: bold;
    color: #40a3bd;
    margin-left: 12px;
  }
`;
