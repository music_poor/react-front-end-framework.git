import styled from 'styled-components';
export const ContainerDiv = styled.div`

  background-size: 100%;
  background-color: #ececec;
  padding: 20px;
  height: 100%;
`;
export const WrapperDiv = styled.div`
  background-color: #fff;
  border-radius: 10px;
  padding-bottom: 10px;
  .inputBtn {
    border: 1px solid #d9d9d9;
    background-color: #f5f5f5;
    border-radius: 5px;
  }
  .ant-collapse-header {
    padding: 12px 20px 8px !important;
    .ant-collapse-arrow {
      margin: -7px 4px 0 0;
    }
  }
`;
export const FormDiv = styled.div`
  padding: 20px 20px 0 20px;
  margin: 10px 20px 20px;
  border-radius: 5px;
  border: 1px solid #e5e5e6;
  .ant-input,
  .ant-form-item .ant-select,
  .ant-form-item .ant-cascader-picker {
    // max-width: 370px;
  }
  textarea.ant-input {
    max-width: 100%;
  }
  .ant-input-number {
    width: 100%;
    // max-width: 370px;
  }
  .ant-picker {
    // max-width: 370px;
  }
`;
export const InputRightWrapDiv = styled.div`
  text-align: right;
  input,
  .ant-select-selection-item,
  .ant-select-selection-placeholder {
    text-align: left;
  }
`;
export const PanelTitleDiv = styled.div`
  font-size: 16px;
  line-height: 40px;
  letter-spacing: 0px;
`;
export const PanelWrapperDiv = styled.div`
  // border: 1px solid #f0f0f0;
  background-color: #fff;
  // border-radius: 5px;
  // padding: 5px 8px;
`;
export const PanelHeaderDiv = styled.div`
  font-size: 16px;
  display: inline-block;
  font-weight: bold;
  span {
    vertical-align: middle;
    padding-right: 12px;
  }
`;
export const SquareGrayWrapper = styled.div`
  border-radius: 5px;
  width: 100%;
  height: calc(100% - 10px);
  padding: 15px 15px 0;
  margin: 8px 0px;
  background-color: #f5f5f5;
  .squareTitle {
    font-weight: bold;
    line-height: 32px;
  }
  .selectedSquare {
    color: #40a3bd;
    display: flex;
    align-items: center;
    .closeIcon {
      font-size: 16px;
      margin-right: 8px;
    }
  }
  .DraggableTags {
    height: calc(100% - 42px);
  }
`;
export const SquareWrapper = styled.div`
  border: 1px solid #e9e9e9;
  border-radius: 5px;
  width: 100%;
  height: calc(100% - 10px);
  padding: 15px 15px 0;
  margin: 8px 0px;
  .squareTitle {
    font-weight: bold;
    line-height: 32px;
  }
  .selectedSquare {
    color: #40a3bd;
    display: flex;
    align-items: center;
    .closeIcon {
      font-size: 16px;
      margin-right: 8px;
    }
  }
  .DraggableTags {
    height: calc(100% - 42px);
  }
  .disabled {
    cursor: not-allowed;
  }
`;
export const SquareTag = styled.div`
  margin: 3px;
  border: 1px dashed #cccccc;
  border-radius: 5px;
  padding: 0 8px;
  line-height: 30px;
  color: #333;
  background: #fdfdfd;
  &:active,
  &:hover {
    border: 1px solid #40a3bd;
    color: #40a3bd;
    transition: all 0.8s;
  }
`;
export const SquareSelectedTag = styled.div`
  border: 1px solid #40a3bd;
  color: #40a3bd;
  margin: 3px 5px 3px 3px;
  padding: 0 8px;
  line-height: 30px;
  border-radius: 5px;
`;
export const RightPopDiv = styled.div`
  width: 150px;
  height: 120px;
  background-color: #fff;
  background-clip: padding-box;
  border-radius: 2px;
  box-shadow: 0 3px 6px -4px #0000001f, 0 6px 16px #00000014,
    0 9px 28px 8px #0000000d;
  // padding: 8px 10px;
  position: absolute;
  opacity: 0;
`;
export const RightPopItemDiv = styled.div`
  padding: 8px 10px;
  cursor: pointer;
  &:hover {
    background-color: #e6f7ff;
    color: #1890ff;
    transition: opacity 0.3s cubic-bezier(0.645, 0.045, 0.355, 1),
      width 0.3s cubic-bezier(0.645, 0.045, 0.355, 1), color 0.3s;
  }
`;
export const FormTitleSpanDiv = styled.div`
  height: 8px;
  background: #9ed0dd;
  margin-top: -9px;
`;
