import app from "@/dva";
import {CrownFilled, SmileFilled} from "@ant-design/icons";
import dynamic from "dva-loading";

// 处理私有model与组件绑定 公共方法
function dynamicLoader(val, modelPath, pagePath) {
  return dynamic({
    val,
    models: () => [import(`${modelPath}`)],
    component: async () => await import(`${pagePath}`),
  });
}
const Form = dynamicLoader(app, "../pages/form/model.ts", "./form");
export default {
  route: {
    // 相当于在layout route={route}
    routes: [
      {
        path: "/",
        key: "login",
        redirect: "./login",
      },
      {
        path: "/AuthCallback",
        key: "AuthCallback",
        redirect: "./AuthCallback",
      },
      {
        path: "/home",
        name: "首页",
        key: "home",
        icon: <SmileFilled />,
        component: "./home",
      },
      {
        path: "/music",
        name: "音乐",
        key: "music",
        icon: <SmileFilled />,
        component: "./music",
      },
      {
        path: "/above",
        name: "Demo",
        key: "aboveBox",
        icon: <CrownFilled />,
        // access: "canAdmin",
        // component: "./above",
        routes: [
          {
            path: "/above",
            name: "d3.JS 实现甘特图",
            key: "above",
            icon: "https://gw.alipayobjects.com/zos/antfincdn/upvrAjAPQX/Logo_Tech%252520UI.svg",
            component: "./above",
          },
          {
            path: "/table",
            key: "table",
            name: "全局model",
            icon: <CrownFilled />,
            component: "./table",
          },
          {
            path: "/form",
            key: "form",
            name: "私有model",
            icon: <CrownFilled />,
            component: Form,
          },
          {
            path: "/demo",
            key: "demo",
            name: "demo",
            icon: <CrownFilled />,
            component: Form,
          },
        ],
      },
    ],
  },
};
