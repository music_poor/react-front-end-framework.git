interface CounterProps {
  routeName: string;
}
const LeftContentRender = (props: CounterProps) => {
  return (
    <>
      {/* 添加您的自定义内容 */}
      <span>{props.routeName}</span>
    </>
  );
};

export default LeftContentRender;
