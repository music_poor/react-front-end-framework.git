// 获取name 使用堆栈不使用递归，非要用递归推荐尾递归
 export function findMenuName(pathname, routes) {
    const stack = [...routes];

    while (stack.length > 0) {
      const currentRoute = stack.pop();

      if (currentRoute.path === pathname) {
        return currentRoute.name;
      }

      if (currentRoute.routes) {
        stack.push(...currentRoute.routes);
      }
    }

    return null;
  }
