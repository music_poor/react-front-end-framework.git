// import {msalInstance} from "@/utils/azureAuth";
import {LogoutOutlined, SettingOutlined, UserOutlined} from "@ant-design/icons";
import {Avatar, Dropdown, Menu, MenuProps} from "antd";
import {connect} from "dva";
import {useContext, useState} from "react";
import {LanguageContext} from "../../LanguageContext";
import {handleLogout} from "../msalConfig";
// 右上角菜单

const RightContentRender = ({account, international, dispatch}) => {
  const {setLanguage} = useContext(LanguageContext);
  const menu: any = (
    <Menu>
      <Menu.Item key='0'>
        <UserOutlined />
        &nbsp; 个人中心
      </Menu.Item>
      <Menu.Item key='1'>
        <SettingOutlined />
        &nbsp; 个人设置
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item
        key='2'
        onClick={() => {
          handleLogout(dispatch);
        }}
      >
        <LogoutOutlined />
        &nbsp; 退出登录
      </Menu.Item>
    </Menu>
  );
  const [open, setOpen] = useState(false);

  const handleMenuClick: MenuProps["onClick"] = e => {
    const {key} = e;
    dispatch({type: "counter/setInternational", payload: key});
    setOpen(false);
    setLanguage(key);
    // 重新加载页面
    // window.location.reload();
  };

  const handleOpenChange = (flag: boolean) => {
    setOpen(flag);
  };
  const items: MenuProps["items"] = [
    {
      label: "简体中文",
      key: "zh-CN",
    },
    {
      label: "English",
      key: "en-US",
    },
  ];

  return (
    <>
      <Dropdown
        menu={{
          items,
          onClick: handleMenuClick,
        }}
        onOpenChange={handleOpenChange}
        open={open}
      >
        <div>{international === "zh-CN" ? "简体中文" : "English"}</div>
      </Dropdown>

      <Avatar size='small' icon={<UserOutlined />} style={{marginLeft: 8, marginRight: 8}} />
      <Dropdown overlay={menu}>
        <div>{account?.name}</div>
      </Dropdown>
    </>
  );
};
export default connect(({counter}) => counter)(RightContentRender);
