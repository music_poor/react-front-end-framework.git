import {PublicClientApplication} from "@azure/msal-browser";

const msalConfig = {
  auth: {
    clientId: process.env.CLIENT_ID,
    authority: "https://login.microsoftonline.com/44c24f42-d49b-4192-9336-5f2989b87356",
    redirectUri: process.env.REDIRECT_URL,
    scope: process.env.SCOPE,
    postLogoutRedirectUri: process.env.REDIRECT_URL, // 添加此行以配置注销后的重定向页面
  },
  cache: {
    cacheLocation: "sessionStorage",
    storeAuthStateInCookie: false,
  },
};
// 实例化msal
export const msalInstance = new PublicClientApplication(msalConfig);

// 获取登录后信息
export const handleRedirect = async (dispatch) => {
  const response = await msalInstance.handleRedirectPromise();
  if (response) {
    // 用户信息
    msalInstance.setActiveAccount(response?.account);
     dispatch({
      type: 'counter/setAccount',
      payload: response?.account,
    });
    return response;
  }
};

// 登录
export const handleLogin = () => {
  msalInstance.loginRedirect({
    scopes: [process.env.SCOPE], // 替换为你需要的权限
  });
};

// 退出
 export const handleLogout = (dispatch) => {
   msalInstance.logoutRedirect();
    dispatch({
      type: 'counter/logout',
    });
    sessionStorage.clear();
};
//  刷新token
export const onAcquireTokenSilent = async (userInfo) => {
  // 通过用户名获取用户信息
  const account = msalInstance.getAccountByUsername(userInfo?.username);
  const accessTokenRequest = {
    scopes: [process.env.SCOPE],
    account,
  };
  // 有了用户信息可以获取token
  const accessTokenResponse = await msalInstance.acquireTokenSilent(accessTokenRequest);
  const accessToken = accessTokenResponse.accessToken;
  return accessToken;
};
