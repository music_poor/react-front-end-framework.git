import {MenuFoldOutlined, MenuUnfoldOutlined} from "@ant-design/icons";
import {ProLayout} from "@ant-design/pro-layout";
import {connect} from "dva";
import React, {
  useEffect,
  useState,
  type JSXElementConstructor,
  type ReactElement,
  type ReactFragment,
  type ReactPortal,
} from "react";
import {Link, Outlet, history, useSelectedRoutes} from "umi";
import proSettings from "../../config/defaultSettings";
import defaultProps from "./_defaultProps";
import LeftContentRender from "./components/LeftContentRender";
import RightContentRender from "./components/rightContentRender";
import {findMenuName} from "./components/roureFunction";
import style from "./index.less";
import {handleRedirect, onAcquireTokenSilent} from "./msalConfig";

interface CounterProps {
  routeName: string;
  token: string;
  installType: boolean;
  account: any;
  dispatch: Function;
}
const BasicLayout: React.FC<CounterProps> = ({routeName, token, installType, account, dispatch}) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [pathname, setPathname] = useState<any>("/login");
  const [collapsed, setCollapsed] = useState<boolean | undefined>(false); // 展开/收起
  const routes = useSelectedRoutes();
  const lastRoute = routes.at(-1);

  useEffect(() => {
    // 鉴权token 拦截
    if (token && lastRoute?.pathname !== "/login") {
      let name = findMenuName(lastRoute?.pathname, defaultProps.route.routes);
      dispatch({type: "counter/routeName", payload: name});
      setPathname(lastRoute?.pathname);
    } else {
      history.push("/login");
    }
  }, [lastRoute?.pathname]);

  const getToken = async () => {
    // 第一次登录调用此处回去token，通过全局 installType
    if (!installType) {
      const res = await handleRedirect(dispatch);
      if (res) {
        dispatch({type: "counter/installType", payload: true});
        onSetToken(res.accessToken, "/home");
      }
    } else if (lastRoute?.pathname !== "/login") {
      // 每次刷新页面调用此处回去token
      const res = await onAcquireTokenSilent(account);
      if (res) {
        onSetToken(res, lastRoute?.pathname);
      }
    }
  };

  const onSetToken = (tokenVal, url) => {
    dispatch({type: "counter/setToken", payload: tokenVal});
    history.push(url);
  };

  useEffect(() => {
    // getToken无返回值所以 void
    void getToken();
  }, []);

  // 自定义collapsed
  const CustomCollapsedButton = (val: boolean | undefined) => {
    const toggleCollapsed = () => {
      setCollapsed(!collapsed);
    };
    return val ? (
      <MenuUnfoldOutlined onClick={toggleCollapsed} className={style.collapsed} />
    ) : (
      <MenuFoldOutlined onClick={toggleCollapsed} className={style.collapsed} />
    );
  };

  // 将topmenu转为side
  interface TopMenu {
    type: "topmenu";
  }

  function convertTopMenuToTop(layout: TopMenu) {
    return {...layout, type: "side"};
  }

  const topMenu: TopMenu = {type: "topmenu"};
  const layout: any = proSettings.layout === "side" ? convertTopMenuToTop(topMenu) : proSettings.layout;

  if (lastRoute?.pathname === "/login") {
    return (
      <div>
        <Outlet />
      </div>
    );
  } else {
    return (
      <div>
        <ProLayout
          navTheme='light'
          style={{minHeight: "100vh"}}
          contentStyle={{margin: 0}}
          disableMobile // 是否禁用移动端模式
          layout={layout} // topmenu | mix | top
          fixSidebar
          collapsed={collapsed}
          collapsedButtonRender={(val: boolean | undefined) => CustomCollapsedButton(val)} // 自定义collapsed 菜单展开/收起icon
          title='JIM'
          logo={
            <img
              onClick={() => {
                history.push("/home");
              }}
              className={style.img}
              src='https://gw.alipayobjects.com/zos/antfincdn/aPkFc8Sj7n/method-draw-image.svg'
              alt=''
            />
          }
          // 菜单
          {...defaultProps}
          location={{
            pathname,
          }}
          // 切换菜单
          // @ts-ignore
          menuItemRender={(
            menuItemProps: {isUrl: any; children: any; path: any},
            defaultDom:
              | string
              | number
              | boolean
              | ReactElement<any, string | JSXElementConstructor<any>>
              | ReactFragment
              | ReactPortal
              | null
              | undefined
          ) => {
            if (token && (menuItemProps.isUrl || menuItemProps.children || !menuItemProps.path)) {
              return defaultDom;
            }
            return <Link to={menuItemProps.path}>{defaultDom}</Link>;
          }}
          rightContentRender={() => <RightContentRender />}
          // headerContentRender={() => <>{routeName}</>}
          headerContentRender={() => (
            <>
              <LeftContentRender routeName={routeName} />
            </>
          )}
        >
          <div className={style.navs}>
            <Outlet />
          </div>
          {/* <div className={style.set}>设置</div> */}
        </ProLayout>
      </div>
    );
  }
};

export default connect(({counter}) => counter)(BasicLayout);
