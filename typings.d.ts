// eslint-disable-next-line linebreak-style
import 'umi/typings';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DvaInstance } from 'dva';

declare module 'dva' {
  export function useDispatch<T = any>(): T;
  export function useSelector<T = any>(
    selector: (state: any) => T,
    equalityFn?: (left: T, right: T) => boolean
  ): T;
}
