module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  // 扩展或继承现有的配置
  extends: ["plugin:react/recommended", "standard-with-typescript", "prettier"],
  overrides: [],
  parser: "@typescript-eslint/parser",  //解析 TypeScript 代码的 JavaScript 解析器
  // files: ["**/*.{ts,tsx}"], // 检测文件
  parserOptions: {
    ecmaFeatures: {
      impliedStrict: true,
      jsx: true,
    },
    ecmaVersion: "latest",
    sourceType: "module",
    project: "tsconfig.json",
    // extraFileExtensions: ['.jsx','.js','.tsx', '.ts',]
  },
  plugins: ["react", "prettier"],
  /**
   * off或者0 => 关闭规则
   * warn或者1 => 开启规则，警告（不影响代码执行）
   * error或者2 => 开启规则，报错（代码不能执行，界面报错）
   */
  rules: {
    // @fixable 必须使用 === 或 !==，禁止使用 == 或 !=，与 null 比较时除外
    eqeqeq: [
      "error",
      "always",
      {
        null: "ignore",
      },
    ],
    "@typescript-eslint/strict-boolean-expressions": 0, //这个规则的作用是确保在条件表达式中使用的布尔值是明确的，而不是模糊的或容易混淆的
    "@typescript-eslint/prefer-optional-chain": 2,// 它建议在可能的情况下使用可选链操作符（?.）来访问对象属性或方法。
    "@typescript-eslint/restrict-template-expressions": [ //它要求在模板字符串中使用的表达式必须是安全的，避免了一些潜在的安全问题。
      2,
      {
        allowNumber: true,
        allowBoolean: true,
        allowAny: true,
        allowNullish: true,
        allowRegExp: true,
      },
    ],
    "@typescript-eslint/semi": [2, "always"], //分隔符 分号
    // "@typescript-eslint/no-useless-constructor": 2,
    "@typescript-eslint/no-empty-function": 2, // 禁止空函数
    "@typescript-eslint/no-var-requires": 0, // 不允许在 import 语句中使用 require 语句
    "@typescript-eslint/explicit-function-return-type": 0, // 显式函数返回类型
    "@typescript-eslint/explicit-module-boundary-types": 0, // 要求导出函数和类的公共类方法的显式返回和参数类型
    "@typescript-eslint/no-explicit-any": 0, // 禁止使用 any 类型
    "@typescript-eslint/no-unused-vars":2, // 禁止定义未使用的变量
    "@typescript-eslint/naming-convention": [
      //命名约定
      2,
      // {
      //   selector: "variable", //表示适用于变量
      //   format: ["camelCase", "UPPER_CASE"],
      //   leadingUnderscore: "forbid",
      //   filter: {
      //     // 忽略部分无法控制的命名
      //     regex: "^(__html|zh_CN|en_US)$",
      //     match: false,
      //   },
      // },
      {
        selector: "property", //表示适用于属性
        format: ["camelCase", "UPPER_CASE", "snake_case", "PascalCase"],
        leadingUnderscore: "forbid",
        filter: {
          // 忽略部分无法控制的命名
          regex: "^(__html|zh_CN|en_US)$",
          match: false,
        },
      },
      {
        selector: "class",
        format: ["PascalCase"],
        leadingUnderscore: "forbid",
      },
      {
        selector: "interface", //生效
        format: ['PascalCase'],
        leadingUnderscore: "forbid",
      },
      {
        selector: "parameter",
        format: ["camelCase"],
        leadingUnderscore: "allow",
        filter: {
          regex: "^(Child|WrappedComponent)$",
          match: false,
        },
      },
      {
        selector: "typeLike",
        format: ["PascalCase"],
        leadingUnderscore: "forbid",
      },
    ],
    // "@typescript-eslint/camelcase": 2,
    "@typescript-eslint/ban-types": [
      // 禁止使用特定类型
      0,
      {
        extendDefaults: true,
        types: {
          "{}": false,
          Function: false,
        },
      },
    ],

    // 换行样式 Windows 操作系统中使用的换行符（新行）通常是回车符(CR) 后跟换行符(LF)，使其成为回车符换行符(CRLF)，而 Linux 和 Unix 使用简单的换行符(LF)。相应的控制序列是"\n"（for LF）和"\r\n"for（CRLF）。
    "linebreak-style": [2, "windows"],
    "no-unused-expressions": [2, {allowShortCircuit: true, allowTernary: true}],//用于检测未使用的表达式。该规则旨在防止意外的副作用（例如赋值、函数调用等）未被使用，从而导致代码错误。
    'no-dupe-class-members': 2, //禁止在类成员中出现重复的名称
    'no-duplicate-imports':2, // 禁止重复导入
    "no-console": 1, //console.log
    "no-debugger": 1, //
    "no-var": 2, // 禁止使用 var
    "linkbreak-style": ["off", "windows"],
    "no-fallthrough": [ //检查 JavaScript 中的 switch 语句是否存在 case 穿透现象 //可以在每个 case 语句中使用 break、return、throw 或 continue 等语句来跳出 switch 语句
      2,
      {
        commentPattern: "fallthrough",
      },
    ],
    "no-const-assign": 2, //禁止修改const声明的变量
    "camelcase": 2, // 变量，方法驼峰
    "global-require": 2, //防止在代码中使用require函数时，将其放在函数内部或局部作用域中，而导致其无法被缓存 正确const fs = require('fs');
    "no-use-before-define": 2, //用于检查 JavaScript 中是否存在变量或函数在定义之前就被使用的情况
    "no-restricted-syntax": 0, //它允许您指定一组被禁止的语法节点类型，以及一个可选的错误消息。比如禁止for...in 遍历对象
    // 'no-restricted-syntax': ['error', {
    //   selector: 'ForInStatement',
    //   message: 'for..in loops are not allowed. Use Object.keys() instead.'
    // }]
    "no-continue": 0,
    "consistent-return": 0,
    "prefer-const": [ //用于检查 JavaScript 中是否有变量被声明为 let，但是在后续的代码中没有被重新赋值，这种情况下建议将变量声明为 const。
      2,
      {
        destructuring: "all",
      },
    ],
    "@typescript-eslint/prefer-nullish-coalescing": 0, //待改
    "key-spacing": [0, {beforeColon: false, afterColon: true}], // 对象字面量中冒号的前后空格
    "object-curly-spacing": [0, "never"], // 大括号内是否允许不必要的空格
    "generator-star-spacing": 0, // 生成器函数*的前后空格
    "comma-spacing": 0, // 逗号前后的空格
    "array-bracket-spacing": [2, "never"], // 是否允许非空数组里面有多余的空格
    "no-trailing-spaces": 2, // 一行结束后面不要有空格
    "no-multiple-empty-lines": 2, // 删除多余的空行
    "no-spaced-func": 2, // 函数调用时 函数名与()之间不能有空格
    "no-regex-spaces": 2, // 禁止在正则表达式字面量中使用多个空格 /foo bar/
    "no-multi-spaces": 2, // 不能用多余的空格,自闭合的标签前要加一个空格
    "no-mixed-spaces-and-tabs": [2, false], // 禁止混用tab和空格
    "require-render-return": 0, // 确保在 render 方法中存在返回值
    "react/react-in-jsx-scope": 0, // 可用空标签<></>
    // "react/jsx-tag-spacing": 2, //强制执行 JSX 元素和它们的属性之间的一致间距。
    "react/self-closing-comp": 2, // 没有子元素的标签请自闭合
    "react/jsx-closing-bracket-location": 1, // 如果组件包含多行属性，在新的一行闭合标签
    "react/no-array-index-key": 2, // 避免使用数组的索引作为 key 属性值, 建议使用稳定的ID
    "react/jsx-boolean-value": 1, // 当属性值为true时可以省略
    "react/jsx-curly-spacing": 1, // 不要在 JSX 的花括号里边加空格
    "react/jsx-closing-bracket-location": 1, // 对齐：遵循以下JSX语法的对齐风格
    "react/no-multi-comp": 0, // 每个文件只包含一个 React 类组件，但是多个函数式组件可以放到一个文件中
    "react/jsx-filename-extension": [2, {extensions: [".tsx", "ts", ".jsx", "js"]}], //用于检查文件扩展名是否符合 JSX 文件的命名约定。
    "react/jsx-indent-props": [2, 2],
    "react/jsx-indent": [2, 2],
    "react/prop-types": 0, //用于检查 React 组件的 props 是否定义了正确的类型。
    "jsx-quotes": [2, "prefer-single"], //规则要求在 JSX 属性中使用双引号作为属性值的引号风格
    "@typescript-eslint/no-var-requires": "off",
    'react/no-children-prop':0,
    '"@typescript-eslint/ban-ts-comment": "off"'
  },
};
