export default {
  define: {
    'process.env.ENV': 'dev',
    'process.env.REDIRECT_URL': 'http://localhost:8020',
    'process.env.Base': 'https://siemensbudgetaryapi.azurewebsites.net',
    'process.env.CLIENT_ID': '28c4f978-8b51-48b0-b23e-9eb2c8d0ddd5',
    'process.env.TENANTID': '44c24f42-d49b-4192-9336-5f2989b87356',
    'process.env.SCOPE': 'api://28c4f978-8b51-48b0-b23e-9eb2c8d0ddd5/default',
  },
};
